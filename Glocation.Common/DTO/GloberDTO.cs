﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glocation.Common.DTO
{
    public class GloberDTO
    {
        public int GloberId { get; set; }

        public string UserName { get; set; }

        public int PositionId { get; set; }
    }
}
