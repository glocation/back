﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Glocation.Common.DTO
{
    public class ProjectsDTO
    {
        public int ProjectId { get; set; }
        public string Description { get; set; }
    }
}
